var searchData=
[
  ['readfromfile',['readFromFile',['../class_order_set.html#a389ef2bf6c3ea2a61048bede32ac1b62',1,'OrderSet']]],
  ['result',['Result',['../class_result.html#a90f44667e23d25ccdeac37f00a74657b',1,'Result']]],
  ['run',['run',['../class_algorithm.html#aa963f8023f50d135c44400c6b9bb07c1',1,'Algorithm::run()'],['../class_dummy_algorithm.html#aa4867ccca424093d6f68086bf1ad5da3',1,'DummyAlgorithm::run()'],['../class_nearest_neighbour_algorithm.html#acd080de8f07ac89e5a196212c5138160',1,'NearestNeighbourAlgorithm::run()'],['../class_nearest_neighbour_annealed_algorithm.html#ae2dbc7f7482231b68a5e928100d05457',1,'NearestNeighbourAnnealedAlgorithm::run()'],['../class_simulated_annealing_algorithm_ex.html#a3be3f9769837aec4afb124cfae0bcabe',1,'SimulatedAnnealingAlgorithmEx::run()']]]
];
