var searchData=
[
  ['getcoordinates',['getCoordinates',['../class_order.html#a91546f38b7c1dba81ff3a8f40fe8dd95',1,'Order']]],
  ['getorders',['getOrders',['../class_order_set.html#aae38e9577625a4651c81f98c6be66562',1,'OrderSet']]],
  ['getparami',['getParamI',['../class_order_set.html#a2f1d2e953eb1ffbf5529c7cfccbc0574',1,'OrderSet']]],
  ['getpathlength',['getPathLength',['../class_average_driving_dist_metric.html#a763b3eae0e694bfac47274b0bef2d72a',1,'AverageDrivingDistMetric::getPathLength()'],['../class_distance_metric.html#a51ecd537403f9702fc6140239cf522cd',1,'DistanceMetric::getPathLength()']]],
  ['getpaths',['getPaths',['../class_result.html#a62fd1466622334e308113beef793071c',1,'Result::getPaths()'],['../class_result.html#a016b3781dfa30b713696e72182cdd3da',1,'Result::getPaths() const']]],
  ['getreport',['getReport',['../class_average_driving_dist_metric.html#ae2eb1e85e95a88812f447295f170a32d',1,'AverageDrivingDistMetric::getReport()'],['../class_distance_metric.html#aa5dc7a5174c34257d1dfb80697c4d44b',1,'DistanceMetric::getReport()'],['../class_dummy_metric.html#a773efcb81a2b94cb9ac6018de3e775d4',1,'DummyMetric::getReport()'],['../class_metric.html#a0609489ece3e3dabc21746c3c320f8bb',1,'Metric::getReport()'],['../class_std_dev_metric.html#a38988e466bbf0b4ef0d38975ef982d4b',1,'StdDevMetric::getReport()']]]
];
