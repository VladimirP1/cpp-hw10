var indexSectionsWithContent =
{
  0: "acdgmnors~",
  1: "admnors",
  2: "g",
  3: "adgmnors",
  4: "acdgmors~",
  5: "n",
  6: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Friends"
};

