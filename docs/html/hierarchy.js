var hierarchy =
[
    [ "Algorithm", "class_algorithm.html", [
      [ "DummyAlgorithm", "class_dummy_algorithm.html", null ],
      [ "NearestNeighbourAlgorithm", "class_nearest_neighbour_algorithm.html", null ],
      [ "NearestNeighbourAnnealedAlgorithm", "class_nearest_neighbour_annealed_algorithm.html", null ],
      [ "SimulatedAnnealingAlgorithm", "class_simulated_annealing_algorithm.html", null ],
      [ "SimulatedAnnealingAlgorithmEx", "class_simulated_annealing_algorithm_ex.html", null ]
    ] ],
    [ "AlgorithmFactory", "class_algorithm_factory.html", null ],
    [ "Metric", "class_metric.html", [
      [ "AverageDrivingDistMetric", "class_average_driving_dist_metric.html", null ],
      [ "DistanceMetric", "class_distance_metric.html", null ],
      [ "DummyMetric", "class_dummy_metric.html", null ],
      [ "StdDevMetric", "class_std_dev_metric.html", null ]
    ] ],
    [ "MetricFactory", "class_metric_factory.html", null ],
    [ "Order", "class_order.html", null ],
    [ "OrderSet", "class_order_set.html", null ],
    [ "Result", "class_result.html", null ]
];