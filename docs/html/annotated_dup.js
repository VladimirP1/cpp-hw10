var annotated_dup =
[
    [ "Algorithm", "class_algorithm.html", "class_algorithm" ],
    [ "AlgorithmFactory", "class_algorithm_factory.html", null ],
    [ "AverageDrivingDistMetric", "class_average_driving_dist_metric.html", "class_average_driving_dist_metric" ],
    [ "DistanceMetric", "class_distance_metric.html", "class_distance_metric" ],
    [ "DummyAlgorithm", "class_dummy_algorithm.html", "class_dummy_algorithm" ],
    [ "DummyMetric", "class_dummy_metric.html", "class_dummy_metric" ],
    [ "Metric", "class_metric.html", "class_metric" ],
    [ "MetricFactory", "class_metric_factory.html", null ],
    [ "NearestNeighbourAlgorithm", "class_nearest_neighbour_algorithm.html", "class_nearest_neighbour_algorithm" ],
    [ "NearestNeighbourAnnealedAlgorithm", "class_nearest_neighbour_annealed_algorithm.html", "class_nearest_neighbour_annealed_algorithm" ],
    [ "Order", "class_order.html", "class_order" ],
    [ "OrderSet", "class_order_set.html", "class_order_set" ],
    [ "Result", "class_result.html", "class_result" ],
    [ "SimulatedAnnealingAlgorithm", "class_simulated_annealing_algorithm.html", null ],
    [ "SimulatedAnnealingAlgorithmEx", "class_simulated_annealing_algorithm_ex.html", "class_simulated_annealing_algorithm_ex" ],
    [ "StdDevMetric", "class_std_dev_metric.html", "class_std_dev_metric" ]
];